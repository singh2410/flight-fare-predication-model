#!/usr/bin/env python
# coding: utf-8

# # Flight Fare Prediction using all regressor Models
# #By- Aarush Kumar
# #Dated: September 19,2021

# In[1]:


from IPython.display import Image
Image(url='https://wallpapers.com/images/high/hd-sunset-airplane-lhlwbwfugiddolzj.jpg')


# In[2]:


import pandas as pd 
import numpy as np 
import seaborn as sns 
import matplotlib.pyplot as plt
import warnings
warnings.filterwarnings('ignore')


# In[3]:


get_ipython().system('pip install openpyxl')


# In[4]:


df = pd.read_excel('/home/aarush100616/Downloads/Projects/Flight Fare Prediction/data/Data_Train.xlsx')


# In[5]:


df


# ## EDA & Visualization

# In[6]:


df.info()


# In[7]:


df.shape


# In[8]:


df.size


# In[9]:


df.isnull().sum()


# In[10]:


df.describe()


# In[11]:


df.describe().T


# In[13]:


missing_values_count = df.isnull().sum()
missing_values_count


# In[14]:


# how many total missing values does it have?
total_cells = np.product(df.shape)
total_missing = missing_values_count.sum()

# percent of data that is missing
percent_missing = (total_missing/total_cells) * 100
print(percent_missing)


# In[15]:


import missingno as msno
msno.heatmap(df)


# In[16]:


#drop the nullvalues
df.dropna(inplace=True)


# In[17]:


df[df.duplicated()]


# In[18]:


df.drop_duplicates(keep='first',inplace=True)


# In[20]:


df.dtypes


# In[21]:


def change_into_datetime(col):
    df[col]=pd.to_datetime(df[col])


# In[22]:


df.columns


# In[23]:


for i in ['Date_of_Journey','Dep_Time', 'Arrival_Time']:
    change_into_datetime(i)


# In[24]:


df.dtypes


# In[25]:


df.value_counts()


# In[26]:


df['journey_day']=df['Date_of_Journey'].dt.day
df['journey_month']=df['Date_of_Journey'].dt.month
df


# In[27]:


df.drop('Date_of_Journey', axis=1, inplace=True)
df.head(2)


# In[28]:


get_ipython().system('pip install plotly matplotlib seaborn --quiet')
import plotly.express as px
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
get_ipython().run_line_magic('matplotlib', 'inline')
sns.set_style('darkgrid')
matplotlib.rcParams['font.size'] = 14
matplotlib.rcParams['figure.figsize'] = (10, 6)
matplotlib.rcParams['figure.facecolor'] = '#00000000'


# In[29]:


fig = px.scatter_3d(df, x='Source', y='Destination', z='Duration')
fig.update_traces(marker_size=3, marker_opacity=0.5)
fig.show()


# In[30]:


def extract_hour(data,col):
    data[col+'_hour']=data[col].dt.hour
    
def extract_min(data,col):
    data[col+'_min']=data[col].dt.minute
    
def drop_col(data,col):
    data.drop(col,axis=1,inplace=True)


# In[31]:


extract_hour(df,'Dep_Time')
extract_min(df,'Dep_Time')


# In[32]:


extract_hour(df,'Arrival_Time')
extract_min(df,'Arrival_Time')


# In[33]:


df.drop(['Dep_Time','Arrival_Time'], axis=1, inplace=True)
df.head()


# In[34]:


duration=list(df['Duration'])
for i in range(len(duration)):
    if len(duration[i].split(' '))==2:
        pass
    else:
        if 'h' in duration[i]: # Check if duration contains only hour
             duration[i]=duration[i] + ' 0m' # Adds 0 minute
        else:
             duration[i]='0h '+ duration[i]


# In[35]:


df['Duration']=duration
df.head()


# In[36]:


def hour(x):
    return x.split(' ')[0][0:-1]

def minutes(x):
    return x.split(' ')[1][0:-1]


# In[37]:


df['dur_hour']=df['Duration'].apply(hour)


# In[38]:


df['dur_min']=df['Duration'].apply(minutes)


# In[39]:


df.head()


# In[40]:


df.dtypes


# In[41]:


df['dur_hour'] = df['dur_hour'].astype(int)
df['dur_min'] = df['dur_min'].astype(int)


# In[42]:


fig = px.histogram(df, 
                   x='Price', 
                   marginal='box', 
                   color='Airline', 
                   color_discrete_sequence=['green', 'grey','red','orange','blue','blueviolet', 'brown', 'burlywood', 'chocolate', 'darkblue','olive','pink'], 
                   title='Airline vs Price')
fig.update_layout(bargap=0.1)
fig.show()


# In[43]:


fig = px.scatter(df, 
                 x='Total_Stops', 
                 y='Price', 
                 color='Source', 
                 opacity=0.8, 
                 hover_data=['Airline'], 
                 title='Total_Stops vs. Price')
fig.update_traces(marker_size=5)
fig.show()


# In[44]:


sns.barplot(data=df, x='Source', y='Price');


# In[45]:


plt.figure(figsize=(15,8))
sns.boxplot(x='Destination',y='Price',data=df.sort_values('Price',ascending=False))


# In[46]:


px.histogram(df, 
             x='journey_day', 
             color='Source', 
             title='Source vs.journey_day')


# In[47]:


fig = px.scatter_3d(df, x='Source', y='Destination', z='Price')
fig.update_traces(marker_size=3, marker_opacity=0.5)
fig.show()


# In[48]:


import plotly.express as px


# In[49]:


df.columns


# In[50]:


df.Destination.value_counts()


# In[51]:


px.scatter(data_frame=df,
                x= 'Price',
                y='journey_day',
                size='journey_month',
                color='Destination',
                title= 'Day Expand and Cost',
                labels= {'Price': 'Cost',
                         'journey_day': 'Day Expand'},
                log_x= True,
                range_y= [0,400],
                hover_name= 'Source',
                animation_frame='Price',
                height= 400,
                size_max=40)


# In[52]:


get_ipython().system('pip install AutoViz')


# In[55]:


get_ipython().system('pip install xlrd')


# In[56]:


import plotly
import plotly.express  as px
from autoviz.AutoViz_Class import AutoViz_Class


# In[57]:


AV= AutoViz_Class()
df_autoviz= AV.AutoViz('/home/aarush100616/Downloads/Projects/Flight Fare Prediction/data/Data_Train.xlsx')


# In[58]:


df['Route'].value_counts()


# In[59]:


df.Additional_Info.value_counts()


# In[60]:


df.drop(['Route','Additional_Info'], axis=1, inplace=True)
df.head(3)


# In[61]:


df.nunique()


# In[62]:


for i in df.columns:
    print('{} contains {} classes'.format(i,len(df[i].value_counts())))


# In[63]:


plt.figure(figsize = (10, 7))
plt.title('Count of flights with different Airlines')
ax=sns.countplot(x = 'Airline', data =df)
plt.xlabel('Airline')
plt.ylabel('Count of flights')
plt.xticks(rotation = 90)
for p in ax.patches:
    ax.annotate(int(p.get_height()), (p.get_x()+0.25, p.get_height()+1), va='bottom',
                    color= 'black')


# In[64]:


df.groupby('Source').sum()


# In[65]:


model_df = df.loc[:,["Airline","Source","Destination","Total_Stops","journey_day","journey_month",
                     "Dep_Time_hour","Dep_Time_min","Arrival_Time_hour","Arrival_Time_min","Price"]]
model_df


# In[66]:


X= model_df.drop(columns=['Price']) # Input columns
y= model_df['Price'] # Target coulumn


# In[67]:


X.head()


# In[68]:


y


# In[69]:


numeric_cols = X.select_dtypes(include=['int64', 'float64']).columns.tolist()
numeric_cols


# In[70]:


categorical_cols = X.select_dtypes('object').columns.tolist()
categorical_cols


# In[71]:


from sklearn.preprocessing import MinMaxScaler


# In[72]:


scaler = MinMaxScaler()


# In[73]:


scaler.fit(model_df[numeric_cols])


# In[74]:


print('Minimum:')
list(scaler.data_min_)


# In[75]:


print('Maximum:')
list(scaler.data_max_)


# In[76]:


model_df[numeric_cols].describe().loc[['min', 'max']]


# In[77]:


X[numeric_cols] = scaler.transform(X[numeric_cols])
X[numeric_cols].describe()


# In[78]:


model_df[categorical_cols].nunique()


# In[79]:


from sklearn.preprocessing import OneHotEncoder


# In[80]:


encoder = OneHotEncoder(sparse=False, handle_unknown='ignore')


# In[81]:


encoder.fit(model_df[categorical_cols])


# In[82]:


encoder.categories_


# In[83]:


encoded_cols = list(encoder.get_feature_names(categorical_cols))
print(encoded_cols)


# In[84]:


X.isna().value_counts()


# In[85]:


X[encoded_cols] = encoder.transform(X[categorical_cols])


# In[86]:


X_train = X[numeric_cols + encoded_cols]
X_test= X[numeric_cols + encoded_cols]


# In[87]:


from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2)


# In[88]:


print('X_train.shape :', X_train.shape)
print('X_test.shape :', X_test.shape)
print('y_train.shape :', y_train.shape)
print('y_test.shape :', y_test.shape)


# In[89]:


X_train.head(3)


# In[90]:


y_train.head(3)


# In[91]:


model_df.Airline.value_counts()


# In[92]:


model_df.Total_Stops.value_counts()


# In[93]:


X.isna().sum()


# In[94]:


model_df.corr()


# In[95]:


fig, ax = plt.subplots(figsize=(10,7)) 
sns.heatmap(model_df.corr(), cmap='Reds', annot=True)
plt.title('Correlation Matrix');


# In[96]:


X_train = X_train[numeric_cols + encoded_cols]
X_test = X_test[numeric_cols + encoded_cols]


# In[97]:


# import necessary modules
from sklearn.metrics import r2_score,mean_absolute_error,mean_squared_error

# call a function named 'ml_model' which will call all Regressor model
def predict(ml_model):

    # Define Models Name
    print('Model: {}'.format(ml_model))

    # fit all models with data
    model= ml_model.fit(X_train,y_train)

    # Model Training Score
    print("Training score: {}".format(model.score(X_train,y_train)))

    # Model Predictions
    predictions = model.predict(X_test)

    # Define r2 score for Regressor Model
    r2score=r2_score(y_test,predictions) 
    print("r2 score: {}".format(r2score))
          
    # Model Evoluation with MAE, MSE, RMSE
    print('MAE:{}'.format(mean_absolute_error(y_test,predictions)))
    print('MSE:{}'.format(mean_squared_error(y_test,predictions)))
    print('RMSE:{}'.format(np.sqrt(mean_squared_error(y_test,predictions))))
     
    sns.distplot(y_test-predictions)


# In[98]:


get_ipython().system('pip install catboost')


# In[99]:


from sklearn.linear_model import  Ridge, Lasso, LinearRegression, SGDRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import GradientBoostingRegressor,RandomForestRegressor,  AdaBoostRegressor, BaggingRegressor
from xgboost import XGBRegressor, XGBRFRegressor
from catboost import CatBoostRegressor
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVR
from lightgbm import LGBMRegressor


# In[100]:


predict(Ridge())


# In[101]:


predict(Lasso())


# In[102]:


predict(LinearRegression())


# In[103]:


predict(DecisionTreeRegressor())


# In[104]:


predict(RandomForestRegressor())


# In[105]:


predict(GradientBoostingRegressor())


# In[106]:


get_ipython().system('pip install lightgbm')


# In[107]:


predict(LGBMRegressor())


# In[108]:


predict(LGBMRegressor())


# In[109]:


predict(XGBRFRegressor())


# In[110]:


predict(KNeighborsRegressor())


# In[111]:


predict(GaussianNB())


# In[112]:


predict(SVR())


# In[113]:


predict(AdaBoostRegressor())


# In[114]:


predict(BaggingRegressor())


# In[115]:


predict(SGDRegressor())


# In[116]:


predict(CatBoostRegressor())


# In[117]:


from catboost import CatBoostRegressor
catboost = CatBoostRegressor(learning_rate=0.1,random_strength=100)
catboost.fit(X_train, y_train)
predictions = catboost.predict(X_test)
r2score=r2_score(y_test,predictions) 
print("r2 score: {}".format(r2score))


# In[118]:


test_preds = catboost.predict(X_test)
test_preds


# In[119]:


test_probs = catboost.predict(X_test)
test_probs


# In[120]:


y_pred = catboost.predict(X_test)


# In[121]:


pred_y = pd.DataFrame({'actual_value': y_test, 'predicted_value': y_pred, 'Difference': y_test-y_pred})
pred_y[:20]


# In[122]:


plt.scatter(y_test, y_pred, alpha = 0.5)
plt.xlabel("y_test")
plt.ylabel("y_pred")
plt.show()


# In[123]:


importance_df = pd.DataFrame({
    'feature': X_train.columns,
    'importance': catboost.feature_importances_
}).sort_values('importance', ascending=False)


# In[124]:


importance_df.head(10)


# In[125]:


import matplotlib.pyplot as plt

plt.title('Feature Importance')
sns.barplot(data=importance_df.head(10), x='importance', y='feature');


# In[126]:


from sklearn.metrics import explained_variance_score
explained_variance_score(y_test, predictions)  


# In[127]:


from sklearn.metrics import max_error
max_error(y_test, predictions)    


# In[128]:


from sklearn.metrics import mean_squared_log_error
mean_squared_log_error(y_test, predictions)


# In[129]:


from sklearn.metrics import mean_poisson_deviance
mean_poisson_deviance(y_test, predictions)


# In[130]:


from sklearn.metrics import mean_gamma_deviance
mean_gamma_deviance(y_test, predictions)


# In[131]:


catboost.classes_


# In[135]:


def predict_input(single_input):
    input_df = pd.DataFrame([single_input])
    input_df[numeric_cols] = scaler.transform(input_df[numeric_cols])
    input_df[encoded_cols] = encoder.transform(input_df[categorical_cols])
    X_input = input_df[numeric_cols + encoded_cols]
    pred = catboost.predict(X_input)[0]
    
    return pred


# In[133]:


new_input = {'Airline': 'IndiGo',
             'Source': 'Banglore',
             'Destination': 'New Delhi',
             'Total_Stops': 'non-stop',
             'journey_day': 24,
             'journey_month': 3,
             'Dep_Time_hour': 22,
             'Dep_Time_min' : 20,
             'Arrival_Time_hour' : 1,
             'Arrival_Time_min' : 10,
            }


# In[134]:


predict_input(new_input)

